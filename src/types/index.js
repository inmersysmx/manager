export const TAGS = [
{
   id:0,
   name: "AR",
  },
  {
   id:1,
   name: "VR",
  },
  {
   id:2,
   name: "Móvil",
  },
  {
   id:3,
   name: "Web",
  },
]

   
export const GET_PROJECTS = "GET_PROJECT";
export const ADD_PROJECT = "ADD_PROJECT";
export const VALIDATE_PROJECT = "VALIDATE_PROJECT";
export const DELETE_PROJECT = "DELETE_PROJECT";
export const SELECT_PROJECT = "SELECT_PROJECT";
export const SELECT_PROJECT_TO_DELETE = "SELECT_PROJECT_TO_DELETE";
export const UPDATE_PROJECT = "UPDATE_PROJECT";
export const CLEAN_PROJECT = "CLEAN_PROJECT";
export const GET_PROJECTS_BY_TYPE = "GET_PROJECTS_BY_TYPE";
export const TOGGLE_SIDEBAR = "TOGGLE_SIDEBAR";
export const TOGGLE_DELETE_MODAL = "TOGGLE_DELETE_MODAL";
export const FILTER_PROJECTS = "FILTER_PROJECTS";


export const REGISTRO_EXITOSO = "REGISTRO_EXITOSO";
export const REGISTRO_ERROR = "REGISTRO_ERROR";
export const OBTENER_USUARIO = "OBTENER_USUARIO";
export const LOGIN_EXITOSO = "LOGIN_EXITOSO";
export const LOGIN_ERROR = "LOGIN_ERROR";
export const CERRAR_SESION = "CERRAR_SESION";



export const MOSTRAR_ALERTA = "MOSTRAR_ALERTA";
export const OCULTAR_ALERTA = "OCULTAR_ALERTA";


export const COLLECTION_WEB = "web"
