import firebase from 'firebase'
import 'firebase/firestore'
const firebaseConfig = {
    apiKey: "AIzaSyAGMtDPYMYo0GN9NdD4K5A4-TqHWH5Rdpg",
    authDomain: "cms-inmersys.firebaseapp.com",
    projectId: "cms-inmersys",
    storageBucket: "cms-inmersys.appspot.com",
    messagingSenderId: "787103763703",
    appId: "1:787103763703:web:dd5b7c6a5d7b490db297f2"
  };
  // Initialize Firebase
export const fb = firebase.initializeApp( firebaseConfig );
export const db = fb.firestore()
export const auth = fb.auth()
export const storage = fb.storage()
