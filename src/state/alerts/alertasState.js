import React, { useReducer } from "react";
  import { toast } from 'react-toastify';

// import alertaReducer from "./alertasReducer";
import alertaContext from "./alertasContext";

const AlertaState = (props) => {
  // const initialState = {
    // alerta: null,
  // };

  // const [state, dispath] = useReducer(alertaReducer, initialState);

  //Functions

  const mostrarAlerta = ( msg, type ) => {
    if(type === "success")
      toast.success(msg)
    else if(type === "warning")
      toast.warn(msg)
    else if(type === "info")
      toast.info( msg )
    else if(type === "error")
      toast.error( msg )
    else
      toast.dark( msg )
      
  };

  return (
    <alertaContext.Provider
      value={{
        mostrarAlerta,
      }}
    >
      {props.children}
    </alertaContext.Provider>
  );
};

export default AlertaState;
