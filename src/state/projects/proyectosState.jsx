import React, { useContext, useReducer, useEffect } from 'react'
import { db } from '../../config/fiebase.config'

import {
  ADD_PROJECT,
  DELETE_PROJECT,
  CLEAN_PROJECT,
  GET_PROJECTS,
  VALIDATE_PROJECT,
  GET_PROJECTS_BY_TYPE,
  SELECT_PROJECT,
  SELECT_PROJECT_TO_DELETE,
  TOGGLE_SIDEBAR,
  TOGGLE_DELETE_MODAL,
  FILTER_PROJECTS,
  //Colecciones
  COLLECTION_WEB
} from '../../types'

// import clienteFirebase from "../../config/fiebase.config";
import ProyectosContext from './proyectosContext'
import AuthContext from '../auth/auth.context'
import ProyectosReducer from './proyectosReducer'
import AlertasContext from '../alerts/alertasContext'

const ProjectsState = props => {
  const authContext = useContext(AuthContext)
  const alertasContext = useContext(AlertasContext)
  const { usuario } = authContext
  const { mostrarAlerta } = alertasContext

  const initialState = {
    projects: [],
    errorproject: null,
    selectedProject: null,
    selectedProjects: [],
    selectedProjectsToDelete: [],
    isSideBarVisible: false,
    isDeleteModalVisible: false,
    query:""
  }

  const [state, dispatch] = useReducer(ProyectosReducer, initialState)

  useEffect(() => {
    getProjects()
  }, [])

  const getProjects = async (collection=COLLECTION_WEB) => {
    try {
      db.collection(collection).onSnapshot(querySnapshotProjects => {
        const projects =  []
        querySnapshotProjects.forEach(doc => {
          console.log("SDAFASFSDAF",doc)
          projects.push({...doc.data(),id:doc.id})
        } )
        dispatch({
          type: GET_PROJECTS,
          payload: projects,
        } );
        
      })
      
    } catch (error) {
      console.log('error al obtener los proyectos', error)
      // mostrarAlerta(error.response);
      console.log(error.response.request)
    }
  }

  const getProjectsByType = tipo => {
    dispatch({ type: GET_PROJECTS_BY_TYPE, payload: tipo })
  }
  const addProject = async (project, callback,collection=COLLECTION_WEB) => {
    try {
      console.log('Proeycto a enviar ', project)
      project.creador = usuario.uid
      project.create = new Date().toDateString()	
      //Se sacan los datos que no son reelevantes ya que vienen del state
      const { file, isPreviewImage, ...dataToSend } = project
      await db
        .collection(collection)
        .doc()
        .set(dataToSend)
        dispatch({ type: ADD_PROJECT, payload: project })
        mostrarAlerta(`${project.title} agregado exitosamente.`, 'success')
        
        if (callback) callback()
    } catch (error) {
      mostrarAlerta(
        `Ah ocurrido un error, vuelva a intentarlo o reinicie el proceso`,
        'error'
        )
        console.error(error)
      }
    }
    const updateProject = async (project,collection=COLLECTION_WEB) => {
      try {
      console.log('Proeycto a actualizar ', project)
      project.creador = usuario.uid
      project.create = new Date().toDateString()	
      //Se sacan los datos que no son reelevantes ya que vienen del state
      const { file, isPreviewImage, ...dataToSend } = project
      await db
      .collection(collection)
      .doc(project.id)
      .update(dataToSend)
      mostrarAlerta(`${project.title} actualizado exitosamente.`, 'success')
      dispatch({ type: CLEAN_PROJECT })
      
    } catch (error) {
      mostrarAlerta(
        `Ah ocurrido un error, vuelva a intentarlo o reinicie el proceso`,
        'error'
      )
      console.error(error)
    }
  }
  const filterProjects = (query,type="title")=>{
    dispatch({ type: FILTER_PROJECTS,payload:{query,type} })

  }
  const validateProject = () => {
  }

  const toggleSidebar = () => {
    dispatch({ type: TOGGLE_SIDEBAR })
  }
  const toggleDeleteModal = () => {
    if(state.selectedProjectToDelete) dispatch({ type: TOGGLE_DELETE_MODAL })
    else{
      alert('no')
    }    
  }

  const selectProject = project => {
    dispatch({ type: SELECT_PROJECT, payload: project })
  }
  const selectProjectToDelete = (project,checked) => {
    dispatch({ type: SELECT_PROJECT_TO_DELETE, payload: {project,checked} })
  }

  const deleteProjects = async (collection=COLLECTION_WEB) => {
    try {
        console.log("Los proyectos a eliminar son ",state.selectedProjectToDelete)
            await db.collection('web').doc(state.selectedProjectToDelete.id).delete()
            dispatch({ type: DELETE_PROJECT, payload: state.selectedProjectToDelete.id })

       mostrarAlerta("Registro eliminados", "success");
    } catch (error) {
      mostrarAlerta("Ocurrio un error", "error");
      console.log(error)
    }
  }

  const editProject = () => {}

  const cleanProject = () => {
    dispatch({ type: CLEAN_PROJECT })
  }

  return (
    <ProyectosContext.Provider
      value={{
        projects: state.projects,
        errorproject: state.errorproject,
        selectedProject: state.selectedProject,
        selectedProjects: state.selectedProjects,
        selectedProjectToDelete: state.selectedProjectToDelete,
        isSideBarVisible: state.isSideBarVisible,
        isDeleteModalVisible: state.isDeleteModalVisible,
        query:state.query,
        getProjects,
        getProjectsByType,
        addProject,
        updateProject,
        validateProject,
        deleteProjects,
        selectProject,
        selectProjectToDelete,
        cleanProject,
        editProject,
        toggleSidebar,
        toggleDeleteModal,
        filterProjects
      }}
    >
      {props.children}
    </ProyectosContext.Provider>
  )
}

export default ProjectsState
