import {
  ADD_PROJECT,
  UPDATE_PROJECT,
  DELETE_PROJECT,
  CLEAN_PROJECT,
  GET_PROJECTS,
  TOGGLE_SIDEBAR,
  VALIDATE_PROJECT,
  GET_PROJECTS_BY_TYPE,
  SELECT_PROJECT,
  SELECT_PROJECT_TO_DELETE,
  TOGGLE_DELETE_MODAL,
  FILTER_PROJECTS
} from '../../types/index'

export default (state, action) => {
  switch (action.type) {
    case GET_PROJECTS:
      return {
        ...state,
        projects: action.payload,
        selectedProjects: action.payload
      }
    case GET_PROJECTS_BY_TYPE:
      return {
        ...state,
        selectedProjects: state.projects.filter(
          proyecto => proyecto.tag === action.payload
        )
      }

    case ADD_PROJECT:
      return {
        ...state,
        projects: [...state.projects, action.payload],
        errorproject: false
      }
    

    case VALIDATE_PROJECT:
      return { ...state, errorproject: true }

    case DELETE_PROJECT:
      const newProjects =  state.projects.filter(
          proyecto => proyecto.id !== action.payload
        ) 
      const newProjectsSelected =  state.selectedProjects.filter(
          proyecto => proyecto.id !== action.payload
        ) 
      return {
        ...state,
        projects: newProjects,
        selectedProject: null,
        selectedProjects: newProjectsSelected,
        isDeleteModalVisible:false,

      }
    case SELECT_PROJECT:
      console.log(action.payload)
      const data = {
        ...state,
        selectedProject: action.payload
      }
      return data
    case SELECT_PROJECT_TO_DELETE:
    console.log(action.payload,"!")
      const dataToDelete = {
        ...state,
        selectedProjectToDelete: action.payload.project
      }
      return dataToDelete
    case CLEAN_PROJECT:
      return {
        ...state,
        selectedProject: null,
        selectedProjectToDelete:null,
        isSideBarVisible:false,
        isDeleteModalVisible: false,
      }
    case FILTER_PROJECTS:
      let arrayFiltered = []
      const {type,query} = action.payload
      if(type ==="title"){
        arrayFiltered = state.projects.filter(project => project.title.toLowerCase().indexOf(query.toLowerCase()) !== -1)  
      }
      else if(type ==="tag"){
        console.log("!TAG",type,query)
        arrayFiltered = state.projects.filter(project => {
          console.log(project.tags)
          if(project.tags.includes(query.toString()))
          return project
        })  
      }
      else if(type ==="all"){
        arrayFiltered = state.projects  
      }
      return {
        ...state,
        selectedProjects: arrayFiltered,
        query
      }
    case TOGGLE_SIDEBAR:
      return {
        ...state,
        isSideBarVisible:!state.isSideBarVisible,
      }
    case TOGGLE_DELETE_MODAL:
      return {
        ...state,
        isDeleteModalVisible:!state.isDeleteModalVisible,
      }
    default:
      return state
  }
}
