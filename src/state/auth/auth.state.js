import React, { useReducer, useContext ,useEffect} from 'react'
import AlertasContext from '../alerts/alertasContext'
import AuthContext from './auth.context'
import AuthReducer from './auth.reducer'
import { auth } from '../../config/fiebase.config'
import { useHistory } from "react-router-dom";

import {
  REGISTRO_EXITOSO,
  OBTENER_USUARIO,
  LOGIN_EXITOSO,
  LOGIN_ERROR,
  CERRAR_SESION
} from '../../types/index'

const AuthState = ({ children }) => {
  const initialState = {
    autenticado: null,
    usuario: null,
    mensaje: null,
    cargando: true
  }
    let history = useHistory();
 

  const [state, dispatch] = useReducer(AuthReducer, initialState)
  const alertsContext = useContext(AlertasContext)
  const { mostrarAlerta } = alertsContext;

  //funciones

   useEffect(() => {
  auth.onAuthStateChanged(function(user) {
  if (user) {
    let data = {}
    data.name= user.displayName
        data.email= user.email
        data.photoUrl= user.photoURL
        data.emailVerified = user.emailVerified
        data.uid = user.uid
        dispatch({
         type: OBTENER_USUARIO,
         payload: data
       });
  } else {
       dispatch({
        type: LOGIN_ERROR
      })   
  }
});
  }, [])

  const registrarUsuarios = async datos => {
    try {
      const respuesta = await auth.createUserWithEmailAndPassword(
        datos.email,
        datos.password
      )
      console.log('REGISTAR USUARIO', respuesta)

      dispatch({ type: REGISTRO_EXITOSO, payload: respuesta.data })
      usuarioAutenticado()
    } catch (error) {
      console.log(error)
      mostrarAlerta(error.response.data.msg,"error")
      mostrarAlerta("Ha ocurrido un error interno,intente más tarde","error")
    }
  }

  //Retorna el usuario autenticado

  const usuarioAutenticado = async () => {
    try {
      const user = auth.currentUser
      const data = {}
      console.log("DATA",user)
      if (user != null) {
        data.name= user.displayName
        data.email= user.email
        data.photoUrl= user.photoURL
        data.emailVerified = user.emailVerified
        data.uid = user.uid
        dispatch({
         type: OBTENER_USUARIO,
         payload: data
       });
  if(data.email === 'isaias@inmersys.com' || data.email === 'jenny.villaseca@mx.naos.com' || data.email === 'adele.momplot@mx.naos.com'  ){
        history.push("/dashboard");
      }else{
        history.push("/home");
      }

      }else{
      dispatch({
        type: LOGIN_ERROR
      })    
      }
    

    } catch (error) {
      console.log(error)
      dispatch({
        type: LOGIN_ERROR
      })
    }
  }
  //Cuando el usuario inicia sesión

  const iniciarSesion = async datos => {
    try {
      const respuesta = await auth.signInWithEmailAndPassword(datos.email,datos.password)

      dispatch({ type: LOGIN_EXITOSO, payload: respuesta.data })
      usuarioAutenticado()
      mostrarAlerta("Bienvenido","success")

      console.log(respuesta)
    } catch (error) {
      // console.log(error.response.data.msg);
      console.log(error)
      if (error.code=='auth/user-not-found') {
        mostrarAlerta("Credenciales no válidas","error")
        dispatch({ type: LOGIN_ERROR, payload: "Ha ocurrido un error" })
      } else {
        mostrarAlerta("Ha ocurrido un error interno,intente más tarde","error")
      }
    }
  }

  // const recoverPassword = async (data) => {
  //   try {
  //     const respuesta = await clienteAxios.post( "/api/auth/recover", data );

  // //     mostrarAlerta("Si existe un usuario con este email llegará un correo para recuperar la contraseña.","success")
  // //     mostrarAlerta("Tienes hasta 1 hora para actualizar realizar la acción.","info")
  //     console.log(respuesta);
  //   } catch (error) {
  //     // console.log(error.response.data.msg);
  //     console.log( error );
  //     if (error.response.data.msg) {
  // //       mostrarAlerta(error.response.data.msg,"error")
  //     } else
  //     {
  // //       mostrarAlerta("Ha ocurrido un error interno,intente más tarde","error")
  //     }
  //   }
  // };
  // const restorePassword = async (data,token) => {
  //   try
  //   {

  //     clienteAxios.defaults.headers.common["x-auth-token"] = token
  //     const respuesta = await clienteAxios.post( "/api/auth/restore", data );
  //     console.log(respuesta);
  // //     mostrarAlerta("Se ha restaurado la contraseña correctamente","success")
  //     delete clienteAxios.defaults.headers.common[ "x-auth-token" ];

  //   } catch (error) {
  //     // console.log(error.response.data.msg);
  //     console.log( error );
  //     if (error.response.data.msg) {
  // //       mostrarAlerta(error.response.data.msg,"error")
  //     } else
  //     {
  // //       mostrarAlerta("Ha ocurrido un error interno,intente más tarde","error")
  //     }
  //   }
  // };

  //Cierra la sesión del usuario

  const cerrarSesion = () => {
    auth.signOut()

    dispatch({ type: CERRAR_SESION })
  }

  return (
    <AuthContext.Provider
      value={{
        cargando: state.cargando,
        token: state.token,
        autenticado: state.autenticado,
        usuario: state.usuario,
        mensaje: state.mensaje,
        usuarioAutenticado,
        registrarUsuarios,
        iniciarSesion,
        cerrarSesion
        // recoverPassword,
        // restorePassword
      }}
    >
      {children}
    </AuthContext.Provider>
  )
}

export default AuthState
