import React, { useContext,useState } from 'react'
import AuthContext from '../../state/auth/auth.context'
import AlertasContext from '../../state/alerts/alertasContext'

import {verifyEmail} from '../../types/utilities'

const LoginLanding = () => {
  const { mostrarAlerta }  = useContext(AlertasContext)


   const {iniciarSesion} = useContext(AuthContext)

  const [ dataLogin, setDataLogin ] = useState( {
    email: "",
    "password":""
  } )
  const {email,password} = dataLogin

  const onChangeData = (e) => {
    setDataLogin( {
      ...dataLogin,
      [e.target.name]:e.target.value
    })
  }


  const validateData = () => {
    let isValid = true
    if (email === ""|| email === null) {
      mostrarAlerta("Ingrese un email")
      isValid = false
    }
    if (password === ""|| password === null) {
      mostrarAlerta("Ingrese un password")
      isValid = false
    }
    if (verifyEmail(!email)) {
      mostrarAlerta("Ingrese un email válido")
      isValid = false
    }
    
    return isValid
  }

  const sendData = (e) => {
    e.preventDefault()
    console.log(dataLogin)
    if (validateData()) {
        iniciarSesion(dataLogin)
    }
  }




  return (

    <div className='w-full h-full flex justify-center items-center  main_login_landing-image'>
      <div className='w-11/12 mx-auto lg:w-3/12 h-3/6 p-14	pb-16	 main-bg-color rounded-lg flex flex-col justify-center shadow-login '>
        <div className="">
          <img
            src='/assets/img/inmer.png'
            alt='Logo'
            className='Inmersyslogo'
          />
        </div>
        <form className='text-white main_font_light' onSubmit={sendData}>
          <div className='pb-7	'>
            <div className='form-group flex flex-col text-black'>
              <label className='pb-2 text-white' htmlFor='exampleInputEmail1'>
                Usuario *
              </label>
              <input
                type='email'
                autofocus="autofocus"
                name="email"
                value={email}
                className='p-2 rounded shadow-lg'
                id='exampleInputEmail1'
                aria-describedby='emailHelp'
                placeholder='Ingresa tu usuario'
                onChange={onChangeData}
              />
            </div>
          </div>
          <div className='form-group flex flex-col shadow-lg pb-7 text-black'>
            <label className='pb-2 text-white' htmlFor='exampleInputEmail1'>
              Contraseña *
            </label>
            <input
              type='password'
                name="password"
                value={password}

              className='p-2 rounded'
              id='exampleInputEmail1'
              aria-describedby='emailHelp'
              placeholder='Ingresa tu contraseña'
                onChange={onChangeData}
            />
          </div>
          <div className='form-group flex flex-col text-black '>
            <input type='submit' className='shadow-lg min-width8rem cursor-pointer mx-auto bg-white p-2 rounded font-extrabold' value="Ingresar"/>
          </div>
        </form>
      </div>
    </div>
  )
}

export default LoginLanding
