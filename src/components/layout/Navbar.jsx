import React, {useContext,useState } from 'react'
import Projects_icon from '../../assets/projects_icon.svg'
import close_icon from '../../assets/off_icon.svg'
import AuthContext from '../../state/auth/auth.context'
import { Link } from 'react-router-dom'

const Navbar = () => {
     const {cerrarSesion} = useContext(AuthContext)
    const [isVisible, setIsVisible] = useState(false)

  const onChangeVisible = () => {
      setIsVisible(!isVisible)
      console.log(isVisible)
  }
  
  return (
    <div className={`h-full ${isVisible ? 'w-60 ':'animate__goLeft'} duration-100 text-white rounded-tr rounded-br	relative main_linear-gradient`}>
      <div className='w-10/12'>
        {
isVisible &&
        <img
          src='/assets/img/inmer2x.png'
          alt='Logo'
          className='Inmersyslogo'
          
        />
        }
    </div>
    <ButtonBurgerNavbar onclick={onChangeVisible} />
      <div>
        {
       isVisible&&   
        <div>

    <ButtonNavbar url="/dashboard" active={true} title="Proyectos"/>
    <ButtonNavbar url="/home" title="Landing"/>
        </div>
      }

      </div>
      <button onClick={cerrarSesion} className="main_font_light flex absolute bottom-0 mb-10 self-end	 justify-center items-center w-full">
        <img
          alt="logo close"
          src={close_icon}
          alt='Logo'
          className={`${isVisible ? 'mr-5' : ''} mt-1`}
        />
        {
isVisible &&
        <span>Cerrar sesión</span>
        }
      </button>
      
    </div>
  )
}
export default Navbar



const ButtonNavbar = ({url,title,active}) => {
 return (
<Link to={url} className={` ${!active ?'opacity-60 ':' secondary_bg-color' } w-full main_font_light flex justify-center items-center p-2 py-3 hover:opacity-80 main_bg-color_hover	cursor-pointer`}>
   <span>
    <img src={Projects_icon} alt="proyecto" className="w-5 h-5 mr-2 mt-2" />
        </span>
        <h4 className="text-lg secondary-text-color font-light">{title }</h4>
      </Link>
 )
}

































const ButtonBurgerNavbar = ({onclick}) => {
 return (
<div className=' w-auto '>
        <div onClick={onclick} className='main-bg-color'>
       <div  className='lds-grid cursor-pointer  main-bg-color'>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
          </div>
        </div>
      </div>
 )
}

