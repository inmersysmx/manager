import {storage} from '../../config/fiebase.config'
import React, { useContext, useState,useEffect } from 'react'
import { TAGS } from '../../types'
import BackgroundModal from './BackgroundModal'
import ProjectsContext from '../../state/projects/proyectosContext'
import AlertsContext from '../../state/alerts/alertasContext'

const ModalAddProject = () => {
  const { mostrarAlerta } = useContext(AlertsContext)
  const { toggleSidebar,addProject,updateProject,selectedProject ,cleanProject} = useContext(ProjectsContext)

  const [dataNewProject, setDataNewProject] = useState(initialStateDataNewProject())
  const [urlPrevisualizacionImage, setUrlPrevisualizacionImagen] = useState(null)
  const [loadingImage, setLoadingImage] = useState(false)
  const [isEdit, setIsEdit] = useState(false)
  const { title, thumbnail, description, url, tags,file,isPreviewImage,user,password} = dataNewProject
  
  useEffect(() => {
    if(selectedProject!== null){
      setDataNewProject(selectedProject)
      setUrlPrevisualizacionImagen(selectedProject.thumbnail)
      setIsEdit(true)
    } else
    {
      setIsEdit(false)
    }
  }, []);

  const onChangeData = e => {
      setLoadingImage(false)
    if( e.target.name === 'thumbnail' )
    {
      const file = e.target.files[0]
      if(file.size >  3000000){
        mostrarAlerta("¡El archivo es muy pesado! Max: 3mb.")
        mostrarAlerta("Puedes redimensionar el tamaño o comprimirla.","info")
        return 
      }
      const refPhoto = storage.ref(`/web/${file.name}`) 
      setDataNewProject({
        ...dataNewProject,
        "thumbnail": refPhoto,
        isPreviewImage:true,
        file
      })
      let reader = new FileReader()
      reader.readAsDataURL(file)
      reader.onload = function () {
        setUrlPrevisualizacionImagen(reader.result)
      }
      return
    }
    setDataNewProject({
      ...dataNewProject,
      [e.target.name]: e.target.value
    })
  }
  const onAddTag = (e)=>{
    let newtags = [...tags]
    if(!tags.includes(e.target.id)){
      newtags.push(e.target.id)    
    }else{
      newtags.splice(tags.indexOf(e.target.id),1)
    }
      setDataNewProject({
      ...dataNewProject,
      'tags': newtags
    })
  }
  const onUploadImage = async () => {
    if (thumbnail) {
      try {
        setLoadingImage(true)
        const snapshot = await thumbnail.put( file )
        const downloadURL = await snapshot.ref.getDownloadURL()
        mostrarAlerta("Imagen subída con éxito.","success")
        setDataNewProject({
          ...dataNewProject,
          'thumbnail':downloadURL,
          'isPreviewImage':false
        })
        setLoadingImage(false)
        console.log('File available at', downloadURL);
        
      } catch (error) {
        setLoadingImage(false)
        mostrarAlerta("Ocurrió un error al subir la imagen","error")
        console.log("ERROR ",error);
      }
    }
  }

  const validateData = () => {
    const errors = []
    let isValid = true
    if (title === '' || title === null) {
      errors.push('Ingrese un titulo')
    }
    if (thumbnail === '' || thumbnail === null) {
      errors.push('Ingrese un password')
    }
    if (description === '' || description === null) {
      errors.push('Ingrese una descripción')
    }
    if (url === '' || url === null) {
      errors.push('Ingrese una url')
    }
    if (user === '' || user === null) {
      errors.push('Ingrese un usuario')
    }
    if (password === '' || password === null) {
      errors.push('Ingrese una contraseña')
    }
    if (tags.length === 0) {
      errors.push('Debe seleccionar almenos un tag')
    }
    if (isPreviewImage) {
      errors.push('Debe subir la imagen antes de enviar')
    }
    if (errors.length !== 0) {
      errors.map(error => mostrarAlerta(error))
      isValid = false
    }
    return isValid
  }
  const onSaveProject = e => {
    e.preventDefault()
    if( validateData() )
    {
      if(!isEdit){
        addProject(dataNewProject) 
      }else{
          updateProject(dataNewProject)
      }
      setDataNewProject(null)
      toggleSidebar()
    }
  }
  const onClose = () => {
    setDataNewProject(initialStateDataNewProject())
  toggleSidebar()
    
  }


  return (
    <BackgroundModal onclick={toggleSidebar}>
      <div className='w-full h-full relative'>
        <aside className=' overflow-y-scroll main_font_light bg-white rounded-tl-lg rounded-bl-lg sm:w-4/5	md:w-2/5 lg:w-1/5 absolute right-0  h-full px-6 pt-1	 animate__bounceInRight'>
          <div className='flex  justify-between items-center mb-2'>
            <h3 className='font-bold main_font_light text-xl align-text-bottom m-0 text-black'>
              Agregar proyecto
            </h3>
            {/* boton cancelar */}
            <div onClick={onClose} className='p-2 '>
              <button className='text-4xl text-black-800 font-light	cursor-pointer z-50'>
                {' '}
                &#xD7;
              </button>
            </div>
          </div>
          <form onSubmit={onSaveProject}>
            <div className=' flex flex-col mb-2'>
              <label className='' htmlFor='exampleInputEmail1'>
                Nombre del proyecto *
              </label>
              <input
                name='title'
                required
                onChange={onChangeData}
                value={title}
                type='text'
                className='p-2 rounded-lg border-2 border-gray-100	'
                id='exampleInputEmail1'
                aria-describedby='emailHelp'
                placeholder='Ingresa el nombre del proyecto'
              />
            </div>
            <div className=' flex flex-col justify-center items-center mb-3'>
              <label className='pb-2.5' htmlFor='exampleInputEmail1'>
                Imagen *
              </label>
              {urlPrevisualizacionImage && (
                <div className="w-20 mb-3 h-20 relative">
                {loadingImage && <Loader/>}
                <img
                  className='w-20 h-20 rounded'
                    src={urlPrevisualizacionImage}
                    alt="urlPrevisualizacionImage"
                />

                </div>
              )}
              <input
                onChange={onChangeData}
                name='thumbnail'
                accept='image/gif, image/png, image/jpeg'
                type='file'
                className='p-2 rounded-lg border-2 border-gray-100	'
                id='exampleInputEmail1'
                aria-describedby='emailHelp'
                placeholder='Ingresa el nombre del proyecto'
              />
              {isPreviewImage && thumbnail &&
              <input 
                type='button'
                onClick={onUploadImage}
                className='border-2 border-gray-100  cursor-pointer border-gray-500	w-28 mt-1 mx-auto bg-white  rounded font-bold'
                value='Subir'
              />}
            </div>
            <div className='flex flex-col mb-2'>
              <label className='pb-2.5' htmlFor='exampleInputEmail1'>
                Descripcion *
              </label>
              <textarea
                onChange={onChangeData}
                name='description'
                value={description}
                className='border-2 border-gray-100 rounded-lg p-2 form-textarea mt-1 block w-full'
                rows='3'
                placeholder='Agrega una descripción del proyecto.'
                required
              ></textarea>
            </div>
            <div className=' flex flex-col mb-2'>
              <label className='pb-2.5' htmlFor='exampleInputEmail1'>
                Link *
              </label>
              <input
                onChange={onChangeData}
                type='url'
                value={url}
                className='p-2 rounded-lg border-2 border-gray-100	'
                name='url'
                aria-describedby='emailHelp'
                placeholder='Pega el enlace del proyecto'
                required
              />
            </div>
            <div className=' flex flex-col mb-2'>
              <label className='pb-2.5' htmlFor='exampleInputEmail1'>
                Usuario *
              </label>
              <input
                onChange={onChangeData}
                type='text'
                value={user}
                className='p-2 rounded-lg border-2 border-gray-100	'
                name='user'
                aria-describedby='userHelp'
                placeholder='Indica el usuario o correo de acceso.'
                required
              />
            </div>
            <div className=' flex flex-col mb-2'>
              <label className='pb-2.5' htmlFor='exampleInputEmail1'>
                Contraseña *
              </label>
              <input
                required
                onChange={onChangeData}
                type='password'
                value={password}
                className='p-2 rounded-lg border-2 border-gray-100	'
                name='password'
                aria-describedby='userHelp'
                placeholder='Indica el usuario o correo de acceso.'
              />
            </div>
            <div className=' flex flex-col mb-7'>
              <label className='pb-2.5' htmlFor='exampleInputEmail1'>
                Selecciona una o más categorias *
              </label>
              <div>
                {TAGS.map(tag => (
                  <button

                    name={tag.name}
                    key={tag.id}
                    id={tag.id}
                    type='button'
                    onClick={onAddTag}
    
                    className={`${tags.includes(tag.id.toString()) ? 'bg-gray-300 text-gray-600' : ''} border border-gray-200   text-gray-400 rounded-md px-4 py-2 m-2 transition duration-500 ease select-none  focus:outline-none focus:shadow-outline`}
                  >
                    {tag.name}
                  </button>
                ))}
              </div>
            </div>
            <div className=' flex mt-7 justify-start'>
              <input
                type='submit'
                className='push cursor-pointer main_font_bold border-2   border-gray-500	  w-4/12 mx-auto bg-white hover:bg-gray-100 p-1 rounded font-bold'
                value={`${!isEdit? 'Guardar': 'Actualizar'}`}
              />
              <input
                type='button'
                className=' push main_font_bold cursor-pointer border-2 border-gray-500	 w-4/12 mx-auto bg-white hover:bg-gray-100 p-1 rounded font-bold'
                value='Cancelar'
                onClick={cleanProject}
              />
            </div>
          </form>
        </aside>
      </div>
    </BackgroundModal>
  )
}

const initialStateDataNewProject = () => {
  return {
    title: '',
    thumbnail: null,
    description: '',
    url: '',
    user: '',
    password: '',
    tags: [],
    file: null,
    isPreviewImage:true
  }
}

export default ModalAddProject





const Loader = () => {
 return (
<div className=' w-full h-full absolute flex justify-center items-center '>
          <div className='lds-grid p-8 '>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
          </div>
      </div>
 )
}