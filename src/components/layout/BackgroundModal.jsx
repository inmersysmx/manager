import React from 'react';

const BackgroundModal = ({children,onclick}) => {
 return (
  <div className="fixed main_bg-modal	w-full h-full z-10" >
   {children}
  </div>
  );
}
 
export default BackgroundModal;