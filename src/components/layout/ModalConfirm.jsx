import React,{useContext} from 'react'
import BackgroundModal from './BackgroundModal'

const ModalConfirm = ({onCancel,onClose,onSuccess,title,subtitle}) => {

  return (
    <BackgroundModal>
      <div className='w-full h-full flex justify-center items-center'>
        <div className='w-10/12 md:w-5/12  xl:w-3/12 h-1/5 bg-white shadow-login	p-4 rounded-lg flex flex-col justify-between'>
          <div className='flex  justify-between items-center h-2/6'>
            <h3 className='font-bold main_font_bold text-sm md:text-lg align-text-bottom m-0 text-black'>
              {title}
            </h3>
            {/* boton cancelar */}
            <div className=' '>
              <button onClick={onClose} className='text-4xl text-black-800 font-light	cursor-pointer z-50'>
                {' '}
                &#xD7;
              </button>
            </div>
          </div>
          <div className="h-4/6 main_font_light text-xs md:text-base  flex flex-col justify-center ">
            <h4 className="mb-2 md:mb-4">{subtitle}</h4>
            <div className=' flex '>
              <input
              onClick={onSuccess}
                type='submit'
                className=' min-width6rem   text-white font-light cursor-pointer 	 mr-2  mx-auto secondary_bg-color  rounded '
                value='Eliminar'
              />
        <input
         onClick={onCancel}
                type='button'
                className='min-width6rem border-1 cursor-pointer border-gray-500	hover:bg-gray-100   bg-white px-4 py-1 rounded font-light '
                value='Cancelar'
              />
            </div>
          </div>
        </div>
      </div>
    </BackgroundModal>
  )
}

export default ModalConfirm
