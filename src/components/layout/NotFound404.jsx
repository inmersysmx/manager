import React from 'react'

const NotFound = () => {
  return (
   <div className="fixed inset-0 z-0  text-gray-300 flex flex-col justify-center items-center main_linear-gradient">
   <a href="/">
   <img
          src='/assets/img/logoLogin.svg'
          alt='Logo inmersys'
          className=' opacity-30 absolute top-0 left-0'
        />
   </a>
     <h2 className="text-8xl">404</h2>
      <p className="text-4xl">¡Vaya! Parece que no hay nada por aquí. </p>
   </div>
  )
}

export default NotFound
