import React, { useContext, useEffect } from "react";
import { Route, Redirect } from "react-router-dom";
import AuthContext from "../../state/auth/auth.context";
import LoadingFs from './LoadingFs'
import {
  useLocation
} from "react-router-dom";
const RutaPrivada = ({ component: Component, ...props }) => {
  const authContext = useContext(AuthContext);
  const { autenticado, cargando } = authContext;
  let location = useLocation();



  return (
    <>
    {cargando && <LoadingFs  />}
    <Route
      {...props}
      render={(props) =>{
        if(!autenticado && !cargando){
          if(location.pathname ==="/")
          return <Redirect to="/" />
          else if(location.pathname ==="/login")
          return <Redirect to="/login" />
          else{
          return <Redirect to="/" />
          }
        }else if(!cargando){
          return <Component {...props} />
        }
      }
          
      }
    />
    </>
  );
};

export default RutaPrivada;