import React from 'react'
import LogoInmer from '../../assets/LogoInmer.png'
const LoadingFs = () => {
  return (
   <div className="fixed inset-0 z-0 opacity-80  text-gray-300 flex flex-col justify-center items-center main_linear-gradient">
   <img
          src={LogoInmer}
          alt='Logo inmersys'
          className=' opacity-50 animate__spinScale '
        />
   </div>
  )
}

export default LoadingFs
