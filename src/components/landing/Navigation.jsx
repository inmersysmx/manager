import React,{useState,useContext,useEffect} from 'react'
import ProjectsContext from '../../state/projects/proyectosContext'
import { TAGS } from '../../types'

const Navigation = () => {
  
  const [active, setActive] = useState(100)
  const { filterProjects } = useContext(ProjectsContext)
  
  useEffect(() => {
      if(active !== 100){
            filterProjects(active,"tag")
          }else{
            filterProjects(null,"all")
      }
  }, [active])

 return (
    <div className='w-full h-36	flex justify-center items-center'>
      <Boton active={active} setActive={setActive} tag={{
        id:100,
        name:"Todo"
      }} />
      {TAGS.map(tag => (
        <Boton active={active} setActive={setActive} tag={tag} />
      ))}
    </div>
  )
}

const Boton = ({ tag,active,setActive }) => {
  const styleStatus = active === tag.id ? 'border border-gray-200 text-gray-800 main_font_bold' : 'text-gray-600 main_font_light'



  return (
    <button
      type='button'
      className={` ${styleStatus} hover:text-gray-600  rounded-md px-4 py-2 m-2 transition duration-500 ease select-none hover:bg-gray-300 focus:outline-none focus:shadow-outline `}
      onClick={()=> setActive(tag.id)}
    >
      {tag.name}
    </button>
  )
}

export default Navigation
