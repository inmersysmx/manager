import React, { useEffect, useState,useContext } from 'react'
import AuthContext from '../../state/auth/auth.context'

const NavbarLanding = () => {

  const [classToChangeBackground, setClassToChangeBackground] = useState('')
   const {cerrarSesion} = useContext(AuthContext)
  
  useEffect(() => {
    window.addEventListener( 'scroll', () => {
      // When the scroll is greater than 200 viewport height, add the scroll-header class to the header tag
          (window.scrollY >= 350) ?  setClassToChangeBackground('inmersys-bg-color z-50'): setClassToChangeBackground('') 

    } );
  }, [])




  return (
    // <nav className='fixed top-0 right-0 left-0 w-full h-12 inmersys-bg-color   flex justify-end text-white'>
    <nav className={` fixed top-0 right-0 left-0 w-full h-12 flex justify-end text-white ${classToChangeBackground}`}>
      <ul className='list-none	w-full flex justify-end items-center'>
        <li className='w-28 cursor-pointer font-medium'>Proyectos</li>
      </ul>
      <div
      onClick={cerrarSesion}
       className=' w-36 md:w-28 mr-4 cursor-pointer text-center flex justify-center items-center  font-light'>

        <span className='text-center '>Cerrar Sesión</span>
      </div>
    </nav>
  )
}

export default NavbarLanding
