import React, { useState } from 'react'

const ProjectCard = ({project}) => {
  const [isHover, setisHover] = useState(false)
   
 const {
    title,
    description,
    thumbnail,
    user,
    password,
    url,
  } = project
  const [stylesCardImage, setstylesCardImage] = useState({
    backgroundImage:`url("${thumbnail}") `,
    backgroundPosition: 'center'
  })

  return (
    <div classsName=' min-height-20rem animate__fadein'>
      <div className='w-full h-full shadow rounded-lg overflow-hidden'>
        <div
          onMouseEnter={() => {
            setisHover(true)
            setstylesCardImage({
              backgroundColor: 'rgba(70, 28, 172, 0.8)',
              backgroundBlendMode: 'multiply',
              backgroundImage:`url("${thumbnail}") `,
              backgroundPosition: 'center'
            })
          }}
          onMouseLeave={() => {
            setstylesCardImage({
              backgroundImage:`url("${thumbnail}") `,
              backgroundPosition: 'center'
            })
            setisHover(false)
          }}
          className='h-3/5 flex flex-col justify-end preload '
          style={stylesCardImage}
        >
          {isHover && (
            <div className='m-3'>
              <div className=' flex '>
                <input
                  type='submit'
                  className='cursor-pointer   min-width6rem  p-1 text-white font-light cursor-pointer 	 mr-2  mx-auto bg-transparent border-1_white   rounded '
                  value='Copiar credenciales'
                />
                <a
                  href={url}
                  target='_blank'
                  className='min-width6rem cursor-pointer   cursor-pointer	secondary_bg-color  text-white px-4 py-1 rounded font-light '
                  
                >Ir al sitio</a>
              </div>
            </div>
          )}
        </div>
        <div className='h-2/5 p-4'>
          <h4 className='mb-2 main_font_bold'>
            {title.replace(/^\w/, (c) => c.toUpperCase())}
          </h4>
          <p className="main_font_light">
            {description.slice(0,100).replace(/^\w/, (c) => c.toUpperCase())}
          </p>
        </div>
      </div>
    </div>
  )
}

export default ProjectCard



