import React,{useState,useContext} from 'react'
import Search_icon from '../../assets/search_icon.svg'
import ProjectsContext from '../../state/projects/proyectosContext'

const Search = ({customstyles}) => {
  
  const { filterProjects } = useContext(ProjectsContext)
  const [query, setQuery] = useState('')

  const handleFilter = (e)=>{
        setQuery(e.target.value)
        filterProjects(e.target.value)
        console.log(e.target.value)
  }

  return (
    <form className={`${customstyles} flex w-10/12  md:w-5/12 lg:w-3/12  bg-white  justify-center items-center p-1 rounded`}>
      <div className='mr-2 w-1/12 ml-3'>
        <img src={Search_icon} className='input-group-text' id='basic-addon1' alt="Buscar"/>
      </div>
      <input
        onChange={handleFilter}
        value={query}
        type='text'
        className='form-control text-black w-11/12 '
        placeholder='Buscar por título.'
      />
    </form>
  )
}

export default Search
