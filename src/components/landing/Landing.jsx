import React,{useContext} from 'react'
import NavbarLanding from './NavbarLanding'
import Logo from '../../assets/logoLogin.svg'
import Search from './Search'
import ProjectCard from './ProjectCard'
import Navigation from './Navigation'
import ProjectsContext from '../../state/projects/proyectosContext'


const Landing = () => {
   const {
    selectedProjects
  } = useContext(ProjectsContext)


  return (
    <>
        {/* <PopUpConfirmation/> */}

      <div className='relative  '>
        <NavbarLanding />
        {/* Imagen del landing con el buscador */}
        <div className=''>
          <div className='h-30rem w-full flex justify-center items-center   main_login_landing-image_fixed'>
            <img src={Logo} alt='Inmersys' className='w-96' />
            <Search customstyles="absolute bottom-0 bottom-search_custom shadow"  />
          </div>
        </div>
        {/* Imagen del landing con el buscador */}
    </div>
    <Navigation/>
    <div className="w-full px-2  mb-16 sm:px-2  md:px-16  lg:px-24 pt-3 h-full  grid grid-flow-row xs:grid-cols-1 sm:grid-cols-2 md:grid-cols-3	 lg:grid-cols-5 auto-rows-custom	 	 gap-4	">
        {selectedProjects.map( project => <ProjectCard key={project.id} project={project}/> )}  
    </div>
    </>
  )
}

export default Landing

const PopUpConfirmation = ({children,onclick}) => {
 return (
  <div className="fixed w-full h-full inset-0 z-10 flex justify-center items-center" >
    <div class=" secondary_font_light main_text_shadow h-20 w-2/12 rounded-lg text-white main-green_bg-color shadow-lg text-white p-5 box-border flex justify-center items-center">
          <span>Correo electrónico de <span className="font-extrabold"> mazda </span> copiado a su portapapeles</span>
    </div>
  </div>
  );
}