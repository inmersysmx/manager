import React,{useContext} from 'react'
import ProjectsContext from '../../state/projects/proyectosContext'
import edit_icon from '../../assets/edit_icon.svg'

const RowProject = ({ data }) => {
    const {selectProject,toggleSidebar,selectProjectToDelete,selectedProjectToDelete,cleanProject } = useContext(ProjectsContext)

  const {
    title,
    description,
    thumbnail,
    user,
    password,
    url,
    create,
    id
  } = data
  const handleEdit = ()=>{
    selectProject(data)
    toggleSidebar()
  }
  const handleSelect = (e)=>{
    if (e.target.checked) {
      console.log(e.target.checked)
    selectProjectToDelete(data)
    }else{
      cleanProject()
    }
  }

  return (
    <div className='w-full py-2 px-2  hover:bg-gray-100 main_font_light duration-500 grid grid-flow-col  auto-cols-fr border-gray-100	border-t-2 mx-4 '>
      <div className='w-12'>
            <input type="checkbox" onChange={handleSelect} disabled={selectedProjectToDelete &&    selectedProjectToDelete.id !== id} ></input>
      </div>
      <div className=' w-32 h-32'>
        <img className=" rounded-lg w-32 h-32  shadow" src={thumbnail} alt={title} />
      </div>
      <div className='p-1 overflow-hidden flex justify-center items-center relative'>
        {title}</div>
      <div className='p-1 overflow-hidden'>{description}</div>
      <div className='p-1 overflow-hidden flex justify-center hover:text-black items-center '> <a className="cursor-pointer" href={url}>{url}</a> </div>
      <div className='p-1 overflow-hidden flex justify-center items-center'>{user}</div>
      <div className='p-1 overflow-hidden flex justify-center items-center'>{ password }</div>
      <div className='p-1 overflow-hidden flex justify-center items-center'>{create}</div>
      <div className='p-1 opacity-50 hover:opacity-100 overflow-hidden flex justify-center items-center cursor-pointer' onClick={handleEdit}> <img src={edit_icon}/> </div>

    </div>
  )
}

export default RowProject
