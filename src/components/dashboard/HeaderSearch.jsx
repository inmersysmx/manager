import React from 'react';
import Search_icon from '../../assets/search_icon.svg'
import Search from  '../landing/Search'
const HeadeSearch = () => {
 return (  
 <header className='w-full main_gray-bg-color flex justify-center text-white p-2 '>
 <Search/>
      </header>

 );
}
 
export default HeadeSearch;