import React from 'react'
import rowleft from '../../assets/rowleft_icon.svg'
import rowright from '../../assets/rowright_icon.svg'
import rowbottom from '../../assets/rowbottom_icon.svg'

const FooterDashboard = () => {
  return (
    <div className='absolute h-24	border-t-2 border-gray-100 w-full main_font_light bottom-0 left-0 flex justify-between items-center bg-white px-10		'>
      <div className=''>
        <button type='button' className=' push flex justify-center items-center'>
          Filas por página: 5 
          <img className=" ml-2" src={rowbottom}/>
        </button>
      </div>
      <div>
        <span className='mr-5'> 1 - 5 de 15 elementos</span>
        <button className='mr-5 push font-bold text-xl' type='button'>
          <img src={rowleft}/>
        </button>
        <button className='mr-5 push font-bold text-xl' type='button'>
          <img src={rowright}/>
        </button>
      </div>
    </div>
  )
}

export default FooterDashboard
