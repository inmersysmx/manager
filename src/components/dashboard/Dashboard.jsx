import React, { useContext,useEffect } from 'react'
import ModalAddProject from '../layout/ModalAddProject'
import ProjectsContext from '../../state/projects/proyectosContext'
import trash_icon from '../../assets/trash_icon.svg'

import RowProject from './RowProject'
import FooterDashboard from './FooterDashboard'
import SearchBar from './HeaderSearch'
import ModalConfirm from '../layout/ModalConfirm'
import Navbar from '../layout/Navbar'
const Dashboard = () => {
  const {
    isSideBarVisible,
    isDeleteModalVisible,
    toggleSidebar,
    toggleDeleteModal,
    selectedProjects,
    cleanProject,
    deleteProjects,
    selectedProjectToDelete
  } = useContext(ProjectsContext)

  console.log("selectedProjectsToDelete",selectedProjectToDelete)
  useEffect(() => {
    
    console.log("dash",{selectedProjectToDelete})
  }, [selectedProjectToDelete])

  return (
    <>
      {isSideBarVisible && <ModalAddProject />}
      {isDeleteModalVisible && <ModalConfirm onCancel={cleanProject} onClose={toggleDeleteModal} onSuccess={deleteProjects}  title="Eliminar proyecto" subtitle={`
      Estas seguro que quieres eliminar el proyect ${selectedProjectToDelete.title}` }
       />}
      {/* /Tiene el inset por una razón, no quitar, fue un poco sucio pero ojalá si tienes tiempo lo puedas mejorar */}
      <div className='flex h-full w-full fixed inset-0'>
        <Navbar  />
        <div className='  w-full h-full relative text-gray-600	'>
          <SearchBar />

          {/* Header delete */}
          <div className='h-11 w-full p-3 bg-white'>
            <div 
            >
              <button
              className={`${selectedProjectToDelete=== null? 'opacity-50 cursor-default':''} flex justify-center items-center`}
              onClick={toggleDeleteModal}
                type="button">
                <img src={trash_icon} className="pr-2"/>
              <span >Eliminar</span>

              </button>
            </div>
          </div>
          {/* Header delete project */}
          <div className='  h-full px-10'>
            {/* Barra superior */}
            <div className='h-24	 bg-white  flex  items-center justify-between'>
              <h2 className='ml-5 font-bold	text-4xl text-black	main_font_bold opacity-90'>Proyectos</h2>
              <div
                onClick={toggleSidebar}
                className='main_text-color main_bg-color_hover p-4 cursor-pointer hover:text-white duration-100'
              >
                {' '}
                <span className=''> + Agregar Proyecto</span>{' '}
              </div>
            </div>
            {/* Barra superior */}

            <div className=' h-5/6 w-full overflow-y-scroll grid grid-flow-row auto-rows-max'>
              {selectedProjects.map(project => (
                <RowProject key={project.id} data={project} />
              ))}
          
            
            </div>
          </div>

          <FooterDashboard />
        </div>
      </div>
    </>
  )
}

export default Dashboard
