import './App.css'
import './components/layout/Navbar'
import 'react-toastify/dist/ReactToastify.css'
import { BrowserRouter as Router, Route } from 'react-router-dom'

import Dashboard from './components/dashboard/Dashboard'
import Landing from './components/landing/Landing'
import LoginLanding from './components/maincontent/LoginLanding'
import LoginCms from './components/maincontent/LoginCms'
import ProjectsState from './state/projects/proyectosState'
import AuthState from './state/auth/auth.state'
import AlertsState from './state/alerts/alertasState'
import { ToastContainer } from 'react-toastify'
import RutaPrivada from './components/layout/RutaPrivada'
import NoMatch from './components/layout/NotFound404'

function App () {
  return (
    <>
      <ToastContainer
        position='top-right'
        autoClose={4000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />{' '}
      <Router>
        <AlertsState>
          <AuthState>
            <ProjectsState>
              {/* <Navbar /> */}

              <div className=' min-h-screen	h-screen w-full '>
                <Route exact path='/login' component={LoginCms} />
                <Route exact path='/' component={LoginLanding} />
                <RutaPrivada exact path='/dashboard' component={Dashboard} />
                <RutaPrivada exact path='/home' component={Landing} />
                    <Route path="/home/*">
            <NoMatch />
          </Route>  
                    <Route path="/dashboard/*">
            <NoMatch />
          </Route>  
                      </div>
            </ProjectsState>
          </AuthState>
        </AlertsState>
      </Router>
    </>
  )
}

export default App
